const keyName = 'foodName';
const Keystt = 'stt';
const keyImage = 'imageUrl';

class Food {
  late int? STT;
  late String foodName;
  late String? imageUrl;

  Food({
    required this.STT,
    required this.foodName
  });

  Food.fromJson(Map<String, dynamic> json) {
    this.STT = json['stt'] ?? 0;
    this.foodName = json[keyName] ?? '';
  }

  Map<String, dynamic> toJson() {
    return {
      Keystt: STT,
      keyName: foodName,
    };
  }
}