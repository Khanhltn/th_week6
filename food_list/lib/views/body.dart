import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../models/food.dart';
import '../models/pizza.dart';
import '../utils/httphelper.dart';

  // Future<List<Food>> callFood() async {
  //   print("Use HTTPHELPER");
  //   HttpHelper helper = HttpHelper();
  //   List<Food> Foodd = await helper.getFoodList();
  //   return Foodd;
  // }

  Future<List<Pizza>> callPizzas() async {
    print('Use HttpHelper to get list of pizzas...');
    HttpHelper helper = HttpHelper();
    List<Pizza> pizzas = await helper.getPizzaList();
    return pizzas;
  }


class body extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return bodyState();
  }

}

class bodyState extends State<body>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder(
          future: callPizzas(), //readJsonFile(context),
          builder: (BuildContext context, AsyncSnapshot<List<Pizza>> pizzas) {
            return ListView.builder(
                itemCount: pizzas.data?.length ?? 0,
                itemBuilder: (BuildContext context, int index) {
                  var itemData = pizzas.data![index];
                  return Slidable(
                    child: ListTile(
                      title: Text(itemData.pizzaName),
                      subtitle: Text(
                          "${itemData.description } \$ ${itemData.price}"),
                    ),
                    actionPane: SlidableDrawerActionPane(),
                    actionExtentRatio: 0.25,
                    actions: [
                      IconSlideAction(
                        caption: 'Delete',
                        color: Colors.blueAccent,
                        icon: Icons.delete,
                        onTap: () {
                          setState(() {
                            pizzas.data!.remove(pizzas.data![index]);
                          });
                        },
                      )
                    ],
                  );
                }
            );
          },)
    );
  }
}
