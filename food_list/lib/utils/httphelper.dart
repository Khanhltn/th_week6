import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert';

import '../models/food.dart';
import '../models/pizza.dart';

class HttpHelper {
  // final String server = 'er381.mocklab.io';
  // final String path = 'foodlist';
  // final String postPath = 'food';
  // final String putPath = 'food';
  // final String deletePath = 'food';
  final String server = 'rocky.mocklab.io';
  final String path = 'pizzalist';
  final String postPath = 'pizza';
  final String putPath = 'pizza';
  final String deletePath = 'pizza';

  // Future<List<Food>> getFoodList() async {
  //   Uri url = Uri.https(server, path);
  //   http.Response result = await http.get(url);
  //   if (result.statusCode == HttpStatus.ok) {
  //     final jsonResponse = json.decode(result.body);
  //     //provide a type argument to the map method to avoid type error
  //     List<Food> foodd =
  //     jsonResponse.map<Food>((i) => Food.fromJson(i)).toList();
  //     return foodd;
  //   } else {
  //     return [];
  //   }
  // }

  // Future<http.Response> postFood(Food food) {
  //   String post = json.encode(food.toJson());
  //   Uri url = Uri.https(server, postPath);
  //   return http.post(
  //     url,
  //     body: post,
  //   );
  // }

  Future<List<Pizza>> getPizzaList() async {
    Uri url = Uri.https(server, path);
    http.Response result = await http.get(url);
    if (result.statusCode == HttpStatus.ok) {
      final jsonResponse = json.decode(result.body);
      //provide a type argument to the map method to avoid type error
      List<Pizza> pizzas = jsonResponse.map<Pizza>((i) => Pizza.fromJson(i)).toList();
      return pizzas;
    } else {
      return [];
    }
  }

}
